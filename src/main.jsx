import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

// Création d'un objet router avec des configurations de routes
const router = createBrowserRouter([

  { // Indication le chemin de l'URL qui est "/" dans "path", "element" va permettre d'affiche le composant Home
    path: "/",
    element: <Home />,
  },
  {// Indication le chemin de l'URL qui est "/cart" dans "path", "element" va permettre d'affiche le composant Cart
    path: "/cart",
    element: <Cart />,
  },

  {// Indication le chemin de l'URL qui est "/products" dans "path", "element" va permettre d'affiche le composant ProductList
    path: "/products",
    element: <ProductList />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
      {/* Toutes les pages sont affichées dans le navigateur à l'aide de RouterProvider */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
