import { Link } from "react-router-dom";
import "./Navigation.css";

// définition de la fonction "Navigation" pour permettre la navigation entre les pages du site.
function Navigation() {

  //Le rendu représentant une liste de liens de navigation
  return (

    //définition des liens des pages grâce à l'élément "Link"
    <ul className="Navigation">
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/cart">Cart</Link>
      </li>
      <li>
        <Link to="/products">Products</Link>
      </li>
    </ul>
  );
}

export default Navigation;
